package com.somospnt.retry.demoretry.service;

import java.util.Random;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

@Service
public class RetryService {
    
    private final Random random = new Random();

    @Retryable(value = {IllegalStateException.class}, maxAttempts = 5, backoff = @Backoff(delay = 2000))
    public String saludar() {
        System.out.print("Creando saludo....... ");
        
        if (random.nextInt(10) >= 3) {
            System.out.println("ERROR");
            throw new IllegalStateException("No se quiere saludar...");
        }
        
        System.out.println("OK");
        return "Hola, mundo";
    }
    
    @Recover
    public String saludoDefault(Throwable t) {
        System.out.println("Recuperando error: " + t.getMessage());
        return "Hola, mundo (por default)";
    }

}
