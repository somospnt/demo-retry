package com.somospnt.retry.demoretry;

import com.somospnt.retry.demoretry.service.RetryService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableRetry
public class DemoRetryApplication {

    public static void main(String[] args) {
        RetryService retryService = SpringApplication.run(DemoRetryApplication.class, args).getBeanFactory().getBean(RetryService.class);

        try {
            
            String saludo = retryService.saludar();
            System.out.println("Saludo obtenido: " + saludo);
            
        } catch (Exception ex) {
            
            System.out.println("ERROR obteniendo saludo: " + ex.getClass().getName());
            
        } finally {
            System.exit(0);
        }
    }
}
