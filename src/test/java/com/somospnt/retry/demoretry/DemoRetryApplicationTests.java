package com.somospnt.retry.demoretry;

import com.somospnt.retry.demoretry.service.RetryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoRetryApplicationTests {

    @Autowired
    private RetryService retryService;
    
    @Test
    public void saludar() {
        String saludo = retryService.saludar();
        System.out.println("Saludo obtenido: " + saludo);
    }

}
